variable "org" {
  description = "Name of the org"
  type        = string
}
variable "action" {
  description = "Name of the org"
  type        = string
}
variable "product" {
  description = "Name of the product"
  type        = string
}
variable "path" {
  description = "absolute path"
  type        = string
}