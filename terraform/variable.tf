variable "consul_url" {
  description = "Consul Url"
  type        = string
}
variable "consul_provider_token" {
  description = "Token to configure consul as provider"
  type        = string
}