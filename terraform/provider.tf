provider "consul" {
  address = var.consul_url
  token = var.consul_provider_token
}
