module "consul-org1-write" {
  source = "./modules/acl-org"
  org = "cnd"
  path = "terraform/cnd"
  action = "write"
}

module "consul-deploy-write" {
  source = "./modules/acl-org"
  org = "vra"
  path = "history/vraX"
  action = "write"
}